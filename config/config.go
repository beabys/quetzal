package config

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

type Configuration interface {
	SetDefaults(*viper.Viper)
}

type Config struct {
	configImpl Configuration
	viper      *viper.Viper
}

// New return  a New Config
func New() *Config {
	c := &Config{}
	c.viper = viper.New()
	// Enable VIPER to read Environment Variables
	c.viper.AutomaticEnv()
	return c
}

func (c *Config) SetConfigImpl(impl Configuration) *Config {
	c.configImpl = impl
	return c
}

func (c *Config) SetDefaults(v *viper.Viper) {
	// Set Default Stage variable if not exist
	c.viper.SetDefault("stage", "development")
}

// LoadConfig is a function to load the configuration, stored on the config files
// Unmarshalling in the Struct given
func (c *Config) LoadConfigs(configuration interface{}, configFile string) (err error) {
	if c.viper == nil {
		c.viper = viper.New()
	}

	// Set Default values if not defined in config file/secrets
	c.SetDefaults(c.viper)
	if c.configImpl != nil {
		c.configImpl.SetDefaults(c.viper)
	}

	if err != nil {
		return err
	}
	// Load variables from config file if exist
	err = c.getLocalConfigs(configFile, true)
	if err != nil {
		return err
	}

	// merge the env Variables (replace the placeholders)
	c.mergeEnvVariables()

	// Unmarshall viper configs into Config struct
	if err = c.viper.Unmarshal(&configuration); err != nil {
		return fmt.Errorf(fmt.Sprintf("Unable to decode application configs: %s", err.Error()))
	}

	return err
}

func (c *Config) getLocalConfigs(s string, mergeConfigs bool) (err error) {
	path, name, ext := splitConfigName(s)

	// set config file name
	c.viper.SetConfigName(name)

	// set path of the config
	c.viper.AddConfigPath(path)

	// set extension of the file
	c.viper.SetConfigType(ext)

	if mergeConfigs {
		if err := c.viper.MergeInConfig(); err != nil {
			return fmt.Errorf(fmt.Sprintf("Fail to load configs: %s", err.Error()))
		}
		return nil
	}
	if err := c.viper.ReadInConfig(); err != nil {
		return fmt.Errorf(fmt.Sprintf("Fail to load configs: %s", err.Error()))
	}
	return nil
}

// mergeEnvVariables replace placeholders on config files
func (c *Config) mergeEnvVariables() {
	for _, k := range c.viper.AllKeys() {
		value := c.viper.GetString(k)
		if strings.HasPrefix(value, "${") && strings.HasSuffix(value, "}") {
			p := strings.TrimSuffix(strings.TrimPrefix(value, "${"), "}")
			v := os.Getenv(p)
			if len(v) > 0 {
				c.viper.Set(k, v)
			}
		}
	}
}

// MustString returns the value associated with the key as a string or a defualt value if empty string.
func (c *Config) MustString(key, must string) string {
	val := c.viper.GetString(key)
	if val != "" {
		return val
	}
	return must
}

// MustInt returns the value associated with the key int or a defualt value if 0.
func (c *Config) MustInt(key string, must int) int {
	val := must
	if c.viper.IsSet(key) {
		val = c.viper.GetInt(key)
	}
	return val
}

// MustInt32 returns the value associated with the key as a int32 or a defualt value if 0.
func (c *Config) MustInt32(key string, must int32) int32 {
	val := must
	if c.viper.IsSet(key) {
		val = c.viper.GetInt32(key)
	}
	return val
}

// MustInt64 returns the value associated with the key as a int64 or a defualt value if 0.
func (c *Config) MustInt64(key string, must int64) int64 {
	val := must
	if c.viper.IsSet(key) {
		val = c.viper.GetInt64(key)
	}
	return val
}

// MustBool returns the value associated with the key as a int64 or a defualt value if 0.
func (c *Config) MustBool(key string, must bool) bool {
	val := must
	if c.viper.IsSet(key) {
		val = c.viper.GetBool(key)
	}
	return val
}

func splitConfigName(s string) (path, name, ext string) {
	fullExt := filepath.Ext(s)
	ext = strings.TrimPrefix(fullExt, ".")
	path, filename := filepath.Split(s)
	name = strings.TrimSuffix(filename, fullExt)
	return path, name, ext
}
