package quetzal

import (
	"sync"
)

type application struct {
	m sync.Map
}

type App application

// Init a New wrapper of App
func New() *App {
	return &App{}
}

// BootRoutes append routes setted on each handler function,
// This function only works if app is a quetzal App type,
// on the embedded pattern, you cannot use this function,
// however, you can re implement the function in your code
func (app *App) BootRoutes(routes []func(*App)) {
	// routes and middleware are handled inside the handler function
	for _, f := range routes {
		f(app)
	}
}

// GetDependency Retrieves the value of the key without modifying it
func (app *App) GetDependency(key string) interface{} {
	// TODO work with atomic values if we need to write variables using hot changes
	v, ok := app.m.Load(key)
	if !ok {
		return nil
	}
	return v
}

// SetDependency Adds value to the stored underlying value if it exists.
// If it does not exist, the value is assigned to the key.
// If exist the value of the key will be overrided
func (app *App) SetDependency(key string, value interface{}) bool {
	// TODO work with atomic values if we need to write variables using hot changes
	app.m.Store(key, value)
	return true
}

// GetRouter Retrieves the Router if this was setted, if not return nil
func (app *App) GetRouter() *Mux {
	if r := app.GetDependency("router"); r != nil {
		return r.(*Mux)
	}
	return nil
}
