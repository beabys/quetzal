package quetzal

import (
	"fmt"

	"github.com/go-redis/redis"
)

type Redis struct {
	DB     *redis.Client
	config *RedisConfig
}

func NewRedis() *Redis {
	return &Redis{}
}

// Mysql type to connect to Redis using Gorm
type RedisConfig struct {
	Password string
	Host     string
	Port     int
	DBNumber int
}

// SetConfigs is Setter to Set a MysqlConfig struct
func (r *Redis) SetConfigs(c *RedisConfig) *Redis {
	r.config = c
	return r
}

// Connect Create the connection with Redis
func (r *Redis) Connect() error {
	if r.config.Host == "" {
		r.config.Host = "127.0.0.1"
	}
	if r.config.Port == 0 {
		r.config.Port = 6379
	}
	config := &redis.Options{
		Addr: fmt.Sprintf("%s:%d", r.config.Host, r.config.Port), // use default Addr
		DB:   r.config.DBNumber,                                  // use default DB
	}
	if r.config.Password != "" {
		config.Password = r.config.Password
	}

	db := redis.NewClient(config)

	_, err := db.Ping().Result()
	if err != nil {
		return err
	}

	r.DB = db
	return nil
}
