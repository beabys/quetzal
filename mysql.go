package quetzal

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Mysql struct {
	DB     *gorm.DB
	config *MysqlConfig
	sqlDB  *sql.DB
}

func NewMysql() *Mysql {
	return &Mysql{}
}

// Mysql type to connect to Mysql using Gorm
type MysqlConfig struct {
	Username        string
	Password        string
	Host            string
	Port            int
	DBName          string
	LogSQL          bool
	MaxIdleConns    int
	MaxOpenConn     int
	ConnMaxLifetime time.Duration
}

// SetConfigs is Setter to Set a MysqlConfig struct
func (m *Mysql) SetConfigs(c *MysqlConfig) *Mysql {
	m.config = c
	return m
}

func (m *Mysql) SetSqlDB(s *sql.DB) *Mysql {
	m.sqlDB = s
	return m
}

// Connect Create the connection with Mysql Adapter
func (m *Mysql) Connect() error {

	gormConfig := &gorm.Config{}

	if m.config.LogSQL {
		gormConfig.Logger = logger.New(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
			logger.Config{
				SlowThreshold:             time.Second, // Slow SQL threshold
				LogLevel:                  logger.Info, // Log level
				IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
				Colorful:                  false,       // Disable color
			},
		)
	}

	var stringConnection = fmt.Sprintf("%s:%s@tcp(%s:%v)/%s?parseTime=True", m.config.Username, m.config.Password, m.config.Host, m.config.Port, m.config.DBName)
	var db, err = gorm.Open(mysql.Open(stringConnection), gormConfig)
	if err != nil {
		return err
	}
	sqlDB, err := db.DB()
	if err != nil {
		return err
	}

	if err := sqlDB.Ping(); err != nil {
		return err
	}

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	maxIdleConns := 10
	if m.config.MaxIdleConns > 0 {
		maxIdleConns = m.config.MaxIdleConns
	}
	sqlDB.SetMaxIdleConns(maxIdleConns)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	maxOpenConn := 10
	if m.config.MaxOpenConn > 0 {
		maxOpenConn = m.config.MaxOpenConn
	}
	sqlDB.SetMaxOpenConns(maxOpenConn)

	// // SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	if m.config.ConnMaxLifetime > 0 {
		duration := m.config.ConnMaxLifetime
		sqlDB.SetConnMaxLifetime(duration)
	}

	m.DB = db
	return nil
}
